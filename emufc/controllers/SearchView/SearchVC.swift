//
//  SearchVC.swift
//  emufc
//
//  Created by icarotavares on 06/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import RealmSwift

class SearchVC: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, HiddenNavBar {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var searchText: String!
    
    lazy var equipments: Results<Equipment> = {
        realm.objects(Equipment.self).sorted(byKeyPath: "name", ascending: true)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        searchBar.delegate = self

        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
    func showNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavBar()
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: - SearchBar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        if searchText.isEmpty {
            equipments = realm.objects(Equipment.self).sorted(byKeyPath: "name", ascending: true)
            return tableView.reloadData()
        }
        
        let predicate = NSPredicate(format: "name CONTAINS[cd] %@ OR desc CONTAINS[cd] %@", searchText, searchText)
        equipments = realm.objects(Equipment.self).filter(predicate).sorted(byKeyPath: "name", ascending: true)
        tableView.reloadData()
    }
    
    // MARK: - TableView DataSource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return equipments.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SearchTVCell {
            cell.equipmentName.text = equipments.sorted(byKeyPath: "name", ascending: true)[indexPath.row].name
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "equipmentInfo", sender: equipments.sorted(byKeyPath: "name", ascending: true)[indexPath.row])
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "equipmentInfo" {
            if let destVC = segue.destination as? EquipmentInfoVC, let equipment = sender as? Equipment {
                destVC.equipment = equipment
            }
        }
    }
    
}
