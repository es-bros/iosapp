//
//  NavigationBarVC.swift
//  emufc
//
//  Created by icarotavares on 20/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit

class NavigationBarVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.tintColor = UIColor.white

    }
}
