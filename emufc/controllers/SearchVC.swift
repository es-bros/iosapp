//
//  SearchVC.swift
//  emufc
//
//  Created by icarotavares on 06/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let toggleMenuButton = UIButton.init(type: .custom)
        toggleMenuButton.setImage(#imageLiteral(resourceName: "menu-2"), for: UIControlState.normal)
        toggleMenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        toggleMenuButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        let leftNavBarButton = UIBarButtonItem(customView: toggleMenuButton)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        let logoButton = UIButton.init(type: .custom)
        logoButton.setImage(#imageLiteral(resourceName: "ufc-web"), for: UIControlState.normal)
        logoButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        let rightNavBarButton = UIBarButtonItem(customView: logoButton)
        self.navigationItem.rightBarButtonItem = rightNavBarButton
    }

}
