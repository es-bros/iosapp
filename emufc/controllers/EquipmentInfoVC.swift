//
//  EquipmentInfoVC.swift
//  emufc
//
//  Created by icarotavares on 26/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

protocol HiddenNavBar {
    func showNavBar()
}


let offset_HeaderStop:CGFloat = 133.0 // At this offset the Header stops its transformations
let offset_B_LabelHeader:CGFloat = 150.0 // At this offset the Black label reaches the Header
let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label

class EquipmentInfoVC: UIViewController, UIScrollViewDelegate, MFMailComposeViewControllerDelegate {
        
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var header:UIView!
    @IBOutlet var headerLabel:UILabel!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerBlurImageView: UIImageView!
    
    var equipment: Equipment!
    
    @IBOutlet weak var equipmentDescription: UILabel!
    @IBOutlet weak var equipmentNameHeader: UILabel!
    @IBOutlet weak var equipmentNavName: UILabel!
    @IBOutlet weak var responsibleName: UILabel!
    @IBOutlet weak var responsibleEmail: UILabel!
    @IBOutlet weak var responsiblePhone: UILabel!
    @IBOutlet weak var routeButton: UIButton!
    
    func hideNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        //        self.navigationController?.view.backgroundColor = UIColor.clear
        //        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.equipmentNameHeader.text = equipment.name
        self.equipmentNavName.text = equipment.name
        self.equipmentDescription.text = equipment.desc
        self.responsibleName.text = equipment.responsible?.name
        self.responsibleEmail.text = equipment.responsible?.email
        self.responsiblePhone.text = equipment.responsible?.phone
        self.routeButton.setTitle(equipment.place?.name, for: .normal)
        
        if let image = UIImage(named: equipment.image) {
            self.headerImageView.image = image
        } else {
            self.headerImageView.image = #imageLiteral(resourceName: "icon-info")
        }
        
        scrollView.delegate = self
    }
    
    // MARK: - Button Actions
    
    @IBAction func route(_ sender: UIButton) {
        let latitude = equipment?.place?.latitude
        let longitude = equipment?.place?.longitude
        if let lat = latitude, let lon = longitude {
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(NSURL(string:
                        "comgooglemaps://?saddr=&daddr=\(lat),\(lon)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            } else {
                let coordinate = CLLocationCoordinate2DMake(lat, lon)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = equipment.name
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            }
        }
    }
    
    @IBAction func sendFeedback(_ sender: UIButton) {
        let emailAddress = "agsf@fisica.ufc.br"
        
        if !MFMailComposeViewController.canSendMail() {
            let url = URL(string: "mailto:" + emailAddress)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([emailAddress])
        composeVC.setSubject("[EMUFC-Equipamentos] \(equipment.name)")
//        if let place = equipment.place, let responsible = equipment.responsible {
//            composeVC.setMessageBody("Local: \(place.name). Responsável: \(responsible.name)", isHTML: false)
//        }
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // MARK: - Image Effects
    override func viewWillAppear(_ animated: Bool) {
        self.hideNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Header - Blurred Image
        headerBlurImageView.image = UIImage(named: equipment.image)?.blurredImage(withRadius: 10, iterations: 20, tintColor: UIColor.clear)
        
        header.clipsToBounds = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            header.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            //  ------------ Label
            
            let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
            headerLabel.layer.transform = labelTransform
            
            //  ------------ Blur
            
            headerLabel.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
            headerBlurImageView.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
            
            if offset <= offset_HeaderStop {
                header.layer.zPosition = 0
            }else {
                header.layer.zPosition = 2
            }
        }
        
        // Apply Transformations
        
        header.layer.transform = headerTransform
    }

}
