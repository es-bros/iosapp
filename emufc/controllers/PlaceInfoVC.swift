//
//  PlaceInfoVC.swift
//  emufc
//
//  Created by icarotavares on 05/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import RealmSwift

class PlaceInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var place: Place!
    
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.placeName.text = place.name
        
        tableView.estimatedRowHeight = 320
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return place.equipments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "placeInfo", for: indexPath) as? PlaceInfoTVCell {
            cell.placeLabel?.text = self.place.equipments[indexPath.row].name
            cell.accessibilityLabel = String(self.place.equipments[indexPath.row].id)
            return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let acLabel = tableView.cellForRow(at: indexPath)?.accessibilityLabel {
            if let equipmentId = Int(acLabel) {
                let realm = try! Realm()
                let equipment = realm.object(ofType: Equipment.self, forPrimaryKey: equipmentId)
                performSegue(withIdentifier: "equipmentInfo", sender: equipment)
            }
        }
    }
    
    // MARK: - Navigation


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? EquipmentInfoVC {
            if let equipment = sender as? Equipment {
                destVC.equipment = equipment
            }
        }
    }
 

}
