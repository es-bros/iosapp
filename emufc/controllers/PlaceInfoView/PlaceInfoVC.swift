//
//  PlaceInfoVC.swift
//  emufc
//
//  Created by icarotavares on 05/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import RealmSwift

class PlaceInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, HiddenNavBar {
    
    var place: Place!
    var equipments: Results<Equipment>!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        equipments = place.equipments.sorted(byKeyPath: "name", ascending: true)
        tableView.estimatedRowHeight = 0
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func showNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavBar()
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: - TableView DataSource and Delegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return place.name
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font.withSize(17.0)
        header.textLabel?.textColor = UIColor(red:0.99, green:0.60, blue:0.15, alpha:1.0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return equipments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "placeInfo", for: indexPath) as? PlaceInfoTVCell {
            cell.placeLabel?.text = self.equipments[indexPath.row].name
            cell.accessibilityLabel = String(self.equipments[indexPath.row].id)
            return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        if let acLabel = tableView.cellForRow(at: indexPath)?.accessibilityLabel {
            if let equipmentId = Int(acLabel) {
                let equipment = realm.object(ofType: Equipment.self, forPrimaryKey: equipmentId)
                if equipment != nil {
                    performSegue(withIdentifier: "equipmentInfo", sender: equipment)
                }
            }
        }
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "equipmentInfo" {
            if let destVC = segue.destination as? EquipmentInfoVC {
                if let equipment = sender as? Equipment {
                    destVC.equipment = equipment
                }
            }
        }
    }
 

}
