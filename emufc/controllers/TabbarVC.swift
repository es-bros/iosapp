//
//  TabbarVC.swift
//  emufc
//
//  Created by icarotavares on 20/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.tintColor = UIColor(red:0.99, green:0.60, blue:0.15, alpha:1.0)
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor(red:0.71, green:0.71, blue:0.71, alpha:1.0)
        } else {
            // Fallback on earlier versions
        }
        self.tabBar.isTranslucent = false
    
    }

}
