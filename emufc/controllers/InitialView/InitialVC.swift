//
//  InitialViewController.swift
//  emufc
//
//  Created by icarotavares on 02/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift
import Alamofire
import AlamofireObjectMapper

class InitialVC: UIViewController, GMSMapViewDelegate, HiddenNavBar {
    
    var mapView: GMSMapView!
    var locManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Map initialization code
        let camera = GMSCameraPosition.camera(withLatitude: -3.745387, longitude: -38.575737, zoom: 16.0)
        self.mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.compassButton = true
        self.mapView.setMinZoom(16, maxZoom: 18)
        
        self.view = mapView
        DispatchQueue.main.async {
            self.refreshRealmData()
            self.plotMarkersInMap()
        }
        
        
        //Location Manager
        self.locManager = CLLocationManager()
        checkLocationAuthorization()
    }
    
    func showNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavBar()
    }

    
    //Pode ser usado para ir ao campus mais proximo do usuario
    func checkLocationAuthorization() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                self.locManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                return
            }
        }
    }
    
    // MARK: - GoogleMaps DataSource and Delegate
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let infoView = Bundle.main.loadNibNamed("CustomInfoView", owner: self.view, options: nil)!.first! as! CustomInfoView
        let place = realm.object(ofType: Place.self, forPrimaryKey: Int(marker.accessibilityLabel!)!)
        infoView.placeName.text = place?.name
        
        if let equipments = place?.equipments.sorted(byKeyPath: "name", ascending: true) {
            switch equipments.count {
            case 0:
                infoView.equipment1.text = ""
                infoView.equipment2.text = ""
            case 1:
                infoView.equipment1.text = equipments[0].name
                infoView.equipment2.text = ""
            default:
                infoView.equipment1.text = equipments[0].name
                infoView.equipment2.text = equipments[1].name
            }
        } else {
            infoView.equipment1.text = ""
            infoView.equipment2.text = ""
        }

        infoView.layer.cornerRadius = 10
        
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let acessibilityLabel = marker.accessibilityLabel {
            if let placeId = Int(acessibilityLabel) {
                let place = realm.object(ofType: Place.self, forPrimaryKey: placeId)
                if place != nil {
                    performSegue(withIdentifier: "showEquipments", sender: place)
                }
            }
        }
    }
    
    //MARK: - Navigation
    
    func pushAboutViewController() {
        performSegue(withIdentifier: "about", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEquipments" {
            if let destVC = segue.destination as? PlaceInfoVC {
                if let place = sender as? Place {
                    destVC.place = place
                }
            }
        }
    }
    
    // MARK: - Map customized function
    
    func plotMarkersInMap() {
        let places = realm.objects(Place.self)
        
        for place in places {
            let position = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
            let marker = GMSMarker(position: position)
            marker.title = place.name
            marker.accessibilityLabel = String(place.id)
            marker.appearAnimation = .pop
            marker.icon = GMSMarker.markerImage(with: UIColor(red:0.00, green:0.38, blue:0.61, alpha:1.0))
            marker.map = self.mapView
        }
    }

}
