//
//  InitialVCAlamoFireExt.swift
//  emufc
//
//  Created by icarotavares on 08/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift
import Alamofire
import AlamofireObjectMapper

extension InitialVC {
    
    // MARK: - AlamoFire Requests
    
//    func checkDBVersion() {
//        Alamofire.request("http://192.168.0.82:4000/version", headers: headers)
//            .validate(statusCode: 200..<300)
//            .validate(contentType: ["application/json"])
//            .responseObject { (response : DataResponse<DatabaseVersion>) in
//                switch response.result {
//                case .success:
//                    var currentVersion = 0
//                    let localDBVersion = realm.object(ofType: DatabaseVe  rsion.self, forPrimaryKey: 1)
//                    if let localVersion = localDBVersion {
//                        currentVersion = localVersion.current
//                    }
//                    
//                    if let serviceDBVersion = response.result.value?.current {
//                        if serviceDBVersion > currentVersion {
//                            self.refreshRealmData(with: serviceDBVersion)
//                        }
//                    }
//                    
//                case .failure(let error):
//                    print(error)
//                }
//        }
//    }

    func refreshRealmData() {
//        Alamofire.request("http://192.168.0.131:4000/test", headers: headers)
        Alamofire.request("http://emufc-api-alpha.herokuapp.com/api/test", headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseObject { (response : DataResponse<RequestData>) in
                switch response.result {
                case .success:

                    //Capturando objeto DatabaseVersion do JSON
                    guard let databaseVersion = response.result.value?.databaseVersion else {
                        return
                    }
                    
                    //Capturando versão atual do banco local
                    var currentVersion = 0
                    let localDBVersion = realm.object(ofType: DatabaseVersion.self, forPrimaryKey: 1)
                    if let localVersion = localDBVersion {
                        currentVersion = localVersion.current
                    }
                    
                    //Checando se a versão atual do banco local é igual ou superior do servidor
                    if databaseVersion.current <= currentVersion {
                        return
                    }
                    
                    //Limpando dados
                    try! realm.write {
                        realm.deleteAll()
                        realm.create(DatabaseVersion.self, value: ["id": 1, "current" : databaseVersion.current])
                    }
                    
                    //Readicionando os dados
                    if let equipments = response.result.value?.equipments {
                        try! realm.write {
                            for equipment in equipments {
                                realm.create(Equipment.self, value: equipment, update: true)
                            }
                        }
                    }
                    
                    if let responsibles = response.result.value?.responsibles {
                        try! realm.write {
                            for responsible in responsibles {
                                realm.create(Responsible.self, value: responsible, update: true)
                            }
                        }
                    }
                    
                    if let places = response.result.value?.places {
                        try! realm.write {
                            for place in places {
                                realm.create(Place.self, value: place, update: true)
                            }
                        }
                    }
                    
                    //Plotando pinos no mapa
                    self.mapView.clear()
                    self.plotMarkersInMap()
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
}
