//
//  CustomInfoView.swift
//  emufc
//
//  Created by icarotavares on 08/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import UIKit
import RealmSwift

class CustomInfoView: UIView {
    
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var equipment1: UILabel!
    @IBOutlet weak var equipment2: UILabel!

}
