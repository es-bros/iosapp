//
//  Responsible.swift
//  emufc
//
//  Created by icarotavares on 05/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Responsible : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var email = ""
    dynamic var phone = ""
    let equipments = LinkingObjects(fromType: Equipment.self, property: "responsible")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
    }
}
