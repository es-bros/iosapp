//
//  Place.swift
//  emufc
//
//  Created by icarotavares on 05/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Place : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    dynamic var image = ""
    let equipments = LinkingObjects(fromType: Equipment.self, property: "place")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
    
}
