//
//  Data.swift
//  emufc
//
//  Created by icarotavares on 08/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import Foundation
import ObjectMapper

class RequestData : Mappable {
    
    var places: [Place]?
    var equipments: [Equipment]?
    var responsibles: [Responsible]?
    var databaseVersion: DatabaseVersion?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        places <- map["places"]
        equipments <- map["equipments"]
        responsibles <- map["responsibles"]
        databaseVersion <- map["version"]
    }
    
}
