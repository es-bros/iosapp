//
//  DatabaseVersion.swift
//  emufc
//
//  Created by icarotavares on 08/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class DatabaseVersion : Object, Mappable {
    
    dynamic var id = 1
    dynamic var current = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id = 1
        current <- map["current"]
    }
    
}

