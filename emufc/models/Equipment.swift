//
//  Equipment.swift
//  emufc
//
//  Created by icarotavares on 05/06/17.
//  Copyright © 2017 icarotavares. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Equipment : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var desc = ""
    dynamic var image = ""
    dynamic var responsible: Responsible?
    dynamic var place: Place?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let r = Responsible()
        r.id <- map["fk_responsible_id"]
        
        let p = Place()
        p.id <- map["fk_place_id"]
        
        id <- map["id"]
        name <- map["name"]
        desc <- map["description"]
        image = "\(id)"
        responsible = r
        place = p
    }
    
}
